# linux-n1

#### Introduction

Linux kernel build tool chain for Phicomm N1, mainly for armbian.

#### Build requirements

1.  sudo apt-get install -y git fakeroot build-essential ncurses-dev xz-utils libssl-dev bc flex libelf-dev bison libncurses-dev autoconf dkms libudev-dev libpci-dev libiberty-dev u-boot-tools lzop
2.  Download cross compile toolkit from https://releases.linaro.org/components/toolchain/binaries/latest-7/aarch64-linux-gnu/gcc-linaro-7.5.0-2019.12-x86_64_aarch64-linux-gnu.tar.xz, extract it and record the path.

#### Run build

1.  mkdir build
2.  Download linux kernel source from http://cdn.kernel.org/pub/linux/kernel/v5.x/linux-xxx.xz into the created 'build' directory
3.  Run build.sh with 2 parameters. The first parameter is the kernel version you downloaded. The second parameter is the cross compile toolkit path in build requirment 2. For example: 
```
./build.sh 5.10.58 "/opt/gcc-linaro-7.5.0-2019.12-x86_64_aarch64-linux-gnu"
```
4.  Get the kernel/boot files in build/target.$VER

#### Install
1.  Backup your old boot files if it exists (Better not put the backup files in /boot. It usually doesn't have enough space.): 
```
/boot/Image
/boot/s905_autoscript
/boot/emmc_autoscript
/boot/u-boot.ext
/boot/dtb/meson-gxl-s905d-phicomm-n1.dtb
/boot/extlinux
```
2.  Install new boot files
```
dpkg  -i *deb
cp Image /boot/
cp s905_autoscript /boot/
cp emmc_autoscript /boot/
cp u-boot.ext /boot/
cp dtb/meson-gxl-s905d-phicomm-n1.dtb /boot/dtb/
mkdir /boot/extlinux
# for usb only
cp extlinux/extlinux.conf.usb /boot/extlinux/extlinux.conf
# for emmc only
cp extlinux/extlinux.conf.emmc /boot/extlinux/extlinux.conf
```
3.  reboot

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### Refer and thanks to:
1.  https://github.com/SuzukiHonoka/s905d-kernel-precompiled
2.  https://github.com/cattyhouse/N1-install
3.  https://github.com/hexdump0815/u-boot-misc
